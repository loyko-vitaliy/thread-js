import React from 'react';
import PropTypes from 'prop-types';

import CommentForm from '../CommentForm';

const AddComment = ({ postId, addComment }) => {
  const saveComment = async ({ body }, clearForm) => {
    const newComment = await addComment({ postId, body });
    clearForm();
    return newComment;
  };

  return <CommentForm saveComment={saveComment} />;
};

AddComment.propTypes = {
  addComment: PropTypes.func.isRequired,
  postId: PropTypes.string.isRequired
};

export default AddComment;
