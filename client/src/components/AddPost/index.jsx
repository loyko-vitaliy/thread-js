import React from 'react';
import { Segment } from 'semantic-ui-react';
import PropTypes from 'prop-types';

import PostForm from '../PostForm';

const AddPost = ({ addPost, uploadImage }) => {
  const savePost = async (post, clearForm) => {
    const newPost = await addPost(post);
    clearForm();
    return newPost;
  };

  return (
    <Segment>
      <PostForm savePost={savePost} uploadImage={uploadImage} />
    </Segment>
  );
};

AddPost.propTypes = {
  addPost: PropTypes.func.isRequired,
  uploadImage: PropTypes.func.isRequired
};

export default AddPost;
