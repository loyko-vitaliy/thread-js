import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Comment as CommentUI, Icon, Modal } from 'semantic-ui-react';
import { NotificationManager } from 'react-notifications';
import moment from 'moment';

import { getUserImgLink } from 'src/helpers/imageHelper';
import UpdateComment from '../UpdateComment';
import Reaction from '../Reaction';
import styles from './styles.module.scss';

const Comment = ({
  comment,
  updateComment,
  isCommentBelongsToUser,
  deleteComment,
  likeComment,
  dislikeComment,
  loadCommentReactions: getCommentReactions
}) => {
  const { id, body, createdAt, user, likeCount, dislikeCount, commentReactions } = comment;
  const [isCommentEdited, setIsCommentEdited] = useState(false);

  const toggleModal = () => setIsCommentEdited(!isCommentEdited);

  const handleDeleteComment = async () => {
    try {
      await deleteComment(id);
      NotificationManager.info('Comment was deleted successfully!');
    } catch (error) {
      NotificationManager.error('Deleting comment was failed!');
    }
  };

  const likedCommentReactions = commentReactions && commentReactions.filter(({ isLike }) => isLike);
  const dislikedCommentReactions = commentReactions && commentReactions.filter(({ isLike }) => !isLike);

  const loadCommentReactions = () => getCommentReactions(id);

  return (
    <>
      <Modal dimmer="blurring" open={isCommentEdited} onClose={toggleModal}>
        <Modal.Header>Updating the comment</Modal.Header>
        <Modal.Content>
          <UpdateComment id={id} body={body} updateComment={updateComment} closeModal={toggleModal} />
        </Modal.Content>
      </Modal>
      <CommentUI className={styles.comment}>
        <CommentUI.Avatar src={getUserImgLink(user.image)} />
        <CommentUI.Content>
          <CommentUI.Author as="a">
            {user.username}
          </CommentUI.Author>
          <CommentUI.Metadata>
            {moment(createdAt).fromNow()}
          </CommentUI.Metadata>
          <CommentUI.Metadata className={styles.status}>
            {user.status ? user.status : 'no status'}
          </CommentUI.Metadata>
          <CommentUI.Text>
            {body}
          </CommentUI.Text>
          <CommentUI.Actions className={styles.actions}>
            <div>
              <CommentUI.Action onClick={() => likeComment(id)}>
                <Reaction
                  type="like"
                  icon="thumbs up"
                  reactions={likedCommentReactions}
                  reactionsCount={Number(likeCount)}
                  loadReactions={loadCommentReactions}
                />
              </CommentUI.Action>
              <CommentUI.Action onClick={() => dislikeComment(id)}>
                <Reaction
                  type="like"
                  icon="thumbs down"
                  reactions={dislikedCommentReactions}
                  reactionsCount={Number(dislikeCount)}
                  loadReactions={loadCommentReactions}
                />
              </CommentUI.Action>
            </div>
            {isCommentBelongsToUser && (
              <div>
                <CommentUI.Action onClick={toggleModal}>
                  <Icon name="edit" size="large" />
                </CommentUI.Action>
                <CommentUI.Action onClick={handleDeleteComment}>
                  <Icon name="trash alternate" size="large" />
                </CommentUI.Action>
              </div>
            )}
          </CommentUI.Actions>
        </CommentUI.Content>
      </CommentUI>
    </>
  );
};

Comment.propTypes = {
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  updateComment: PropTypes.func.isRequired,
  deleteComment: PropTypes.func.isRequired,
  isCommentBelongsToUser: PropTypes.bool.isRequired,
  likeComment: PropTypes.func.isRequired,
  dislikeComment: PropTypes.func.isRequired,
  loadCommentReactions: PropTypes.func.isRequired
};

export default Comment;
