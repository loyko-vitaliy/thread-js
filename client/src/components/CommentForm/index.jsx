import React, { useState } from 'react';
import { Form, Button } from 'semantic-ui-react';
import { NotificationManager } from 'react-notifications';
import PropTypes from 'prop-types';

const CommentForm = ({ body: commentBody, saveComment }) => {
  const [body, setBody] = useState(commentBody);

  const clearForm = () => setBody('');

  const handleSubmit = async () => {
    if (!body) {
      return;
    }
    try {
      await saveComment({ body }, clearForm);
      NotificationManager.info('Comment was created or updated successfully!');
    } catch (error) {
      NotificationManager.error('Creating or updating comment was failed!');
    }
  };

  return (
    <Form reply onSubmit={handleSubmit}>
      <Form.TextArea
        value={body}
        placeholder="Type a comment..."
        onChange={ev => setBody(ev.target.value)}
      />
      <Button type="submit" content="Post comment" labelPosition="left" icon="edit" primary />
    </Form>
  );
};

CommentForm.defaultProps = {
  body: ''
};

CommentForm.propTypes = {
  saveComment: PropTypes.func.isRequired,
  body: PropTypes.string
};

export default CommentForm;
