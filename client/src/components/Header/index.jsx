import React, { useState, useRef } from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { getUserImgLink } from 'src/helpers/imageHelper';
import { Header as HeaderUI, Image, Grid, Button, Input, Icon } from 'semantic-ui-react';
import { NotificationManager } from 'react-notifications';

import styles from './styles.module.scss';

const Header = ({ user, logout, updateUserInfo }) => {
  const [status, setStatus] = useState(user.status || 'no status');
  const [statusEditable, setStatusEditable] = useState(false);

  const inputRef = useRef();

  const handleOnChangeStatus = event => setStatus(event.target.value);

  const updateUserStatus = async () => {
    try {
      await updateUserInfo(user.id, { status });
      NotificationManager.info('Status updated successfully!');
    } catch (error) {
      NotificationManager.error('Updating status failed!');
    } finally {
      setStatusEditable(!statusEditable);
    }
  };

  const handleEditStatus = () => {
    if (statusEditable) {
      updateUserStatus();
    }

    setStatusEditable(!statusEditable);
    setTimeout(() => inputRef.current.focus(), 0);
  };

  const handleOnKeyDown = event => {
    if (event.keyCode === 13) {
      updateUserStatus();
    }
  };

  return (
    <div className={styles.headerWrp}>
      <Grid centered container columns="2">
        <Grid.Column>
          {user && (
            <NavLink exact to="/">
              <HeaderUI>
                <Image circular src={getUserImgLink(user.image)} />
                <HeaderUI.Content>
                  {user.username}
                  <HeaderUI.Subheader>
                    <Input
                      ref={inputRef}
                      className={styles.statusInput}
                      type="text"
                      size="large"
                      name="status"
                      value={status}
                      onChange={handleOnChangeStatus}
                      onKeyDown={handleOnKeyDown}
                      disabled={!statusEditable}
                      focus={statusEditable}
                      style={{ width: (status.length + 1) * 8 }}
                    />
                    <Icon name="pencil" onClick={handleEditStatus} />
                  </HeaderUI.Subheader>
                </HeaderUI.Content>
              </HeaderUI>
            </NavLink>
          )}
        </Grid.Column>
        <Grid.Column textAlign="right">
          <NavLink exact activeClassName="active" to="/profile" className={styles.menuBtn}>
            <Icon name="user circle" size="large" />
          </NavLink>
          <Button basic icon type="button" className={`${styles.menuBtn} ${styles.logoutBtn}`} onClick={logout}>
            <Icon name="log out" size="large" />
          </Button>
        </Grid.Column>
      </Grid>
    </div>
  );
};

Header.propTypes = {
  logout: PropTypes.func.isRequired,
  user: PropTypes.objectOf(PropTypes.any).isRequired,
  updateUserInfo: PropTypes.func.isRequired
};

export default Header;
