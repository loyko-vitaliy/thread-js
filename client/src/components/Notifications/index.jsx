import { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import io from 'socket.io-client';
import { NotificationManager } from 'react-notifications';
import 'react-notifications/lib/notifications.css';

const Notifications = ({ user, applyPost, applyUpdatedPost, applyDeletedPost, applyUpdatedCommentReact }) => {
  const { REACT_APP_SOCKET_SERVER: address } = process.env;
  const [socket] = useState(io(address));

  useEffect(() => {
    if (!user) {
      return undefined;
    }
    const { id } = user;

    socket.emit('createRoom', id);

    // Post
    socket.on('post_like', message => NotificationManager.info(message));
    socket.on('post_dislike', message => NotificationManager.warning(message));
    socket.on('post_react', react => react.userId !== id && applyUpdatedPost(react.postId));
    socket.on('new_post', post => post.userId !== id && applyPost(post.id));
    socket.on('updated_post', post => post.userId !== id && applyUpdatedPost(post.id));
    socket.on('deleted_post', post => post.userId !== id && applyDeletedPost(post));

    // Comment
    socket.on('comment_like', message => NotificationManager.info(message));
    socket.on('comment_dislike', message => NotificationManager.warning(message));
    socket.on('comment_react', react => react.userId !== id && applyUpdatedCommentReact(react.commentId));
    socket.on('new_comment', comment => comment.userId !== id && applyUpdatedPost(comment.postId));
    socket.on('updated_comment', comment => comment.userId !== id && applyUpdatedPost(comment.postId));
    socket.on('deleted_comment', comment => comment.userId !== id && applyUpdatedPost(comment.postId));

    return () => {
      socket.close();
    };
  });

  return null;
};

Notifications.defaultProps = {
  user: undefined
};

Notifications.propTypes = {
  user: PropTypes.objectOf(PropTypes.any),
  applyPost: PropTypes.func.isRequired,
  applyUpdatedPost: PropTypes.func.isRequired,
  applyDeletedPost: PropTypes.func.isRequired,
  applyUpdatedCommentReact: PropTypes.func.isRequired
};

export default Notifications;
