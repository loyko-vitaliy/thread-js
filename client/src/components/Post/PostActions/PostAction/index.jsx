import React from 'react';
import { Label, Icon } from 'semantic-ui-react';
import PropTypes from 'prop-types';

import styles from './styles.module.scss';

const PostAction = ({ icon, callback }) => (
  <Label
    basic
    size="big"
    as="a"
    className={`${styles.toolbarBtn} ${styles.noPadding}`}
    onClick={callback}
  >
    <Icon name={icon} />
  </Label>
);

PostAction.propTypes = {
  icon: PropTypes.string.isRequired,
  callback: PropTypes.func.isRequired
};

export default PostAction;
