import React from 'react';
import PropTypes from 'prop-types';

import PostAction from './PostAction';
import styles from './styles.module.scss';

const PostActions = ({ handleDelete, handleUpdate }) => {
  const actions = [
    { icon: 'edit', callback: handleUpdate },
    { icon: 'trash alternate', callback: handleDelete }
  ];

  return (
    <span className={styles.actions}>
      {actions.map(({ icon, callback }) => <PostAction icon={icon} callback={callback} key={icon} />)}
    </span>
  );
};

PostActions.propTypes = {
  handleDelete: PropTypes.func.isRequired,
  handleUpdate: PropTypes.func.isRequired
};

export default PostActions;
