import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Card, Image, Label, Icon, Modal } from 'semantic-ui-react';
import moment from 'moment';
import { NotificationManager } from 'react-notifications';

import UpdatePost from '../UpdatePost';
import PostActions from './PostActions';
import Reaction from '../Reaction';
import styles from './styles.module.scss';

const Post = ({
  isPostBelongsToUser,
  post,
  likePost,
  dislikePost,
  toggleExpandedPost,
  sharePost,
  updatePost,
  uploadImage,
  deletePost,
  loadPostReactions: getPostReactions
}) => {
  const { id, image, body, user, likeCount, dislikeCount, commentCount, createdAt, postReactions } = post;
  const date = moment(createdAt).fromNow();

  const [isPostEdited, setIsPostEdited] = useState(false);

  const toggleModal = () => setIsPostEdited(!isPostEdited);

  const handleDeletePost = async () => {
    try {
      await deletePost(id);
      NotificationManager.info('Post was deleted successfully!');
    } catch (error) {
      NotificationManager.error('Deleting post was failed!');
    }
  };

  const handleLikePost = () => likePost(id);
  const handleDislikePost = () => dislikePost(id);
  const loadPostReactions = () => getPostReactions(id);

  const likedPostReactions = postReactions && postReactions.filter(({ isLike }) => isLike);
  const dislikedPostReactions = postReactions && postReactions.filter(({ isLike }) => !isLike);

  return (
    <>
      <Modal dimmer="blurring" open={isPostEdited} onClose={toggleModal}>
        <Modal.Header>Updating the post</Modal.Header>
        <Modal.Content>
          <UpdatePost
            id={id}
            body={body}
            image={image}
            updatePost={updatePost}
            uploadImage={uploadImage}
            closeModal={toggleModal}
          />
        </Modal.Content>
      </Modal>
      <Card style={{ width: '100%' }}>
        {image && <Image src={image.link} wrapped ui={false} />}
        <Card.Content>
          <Card.Meta className={styles.cardMeta}>
            <span className="date">
              posted by
              {' '}
              {user.username}
              {' - '}
              {date}
            </span>
            {isPostBelongsToUser && <PostActions handleUpdate={toggleModal} handleDelete={handleDeletePost} />}
          </Card.Meta>
          <Card.Description>{body}</Card.Description>
        </Card.Content>
        <Card.Content extra>
          <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={handleLikePost}>
            <Reaction
              type="like"
              icon="thumbs up"
              reactions={likedPostReactions}
              reactionsCount={Number(likeCount)}
              loadReactions={loadPostReactions}
            />
          </Label>
          <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={handleDislikePost}>
            <Reaction
              type="dislike"
              icon="thumbs down"
              reactions={dislikedPostReactions}
              reactionsCount={Number(dislikeCount)}
              loadReactions={loadPostReactions}
            />
          </Label>
          <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => toggleExpandedPost(id)}>
            <Icon name="comment" />
            {commentCount}
          </Label>
          <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => sharePost(id)}>
            <Icon name="share alternate" />
          </Label>
        </Card.Content>
      </Card>
    </>
  );
};

Post.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  updatePost: PropTypes.func.isRequired,
  uploadImage: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired,
  isPostBelongsToUser: PropTypes.bool.isRequired,
  loadPostReactions: PropTypes.func.isRequired
};

export default Post;
