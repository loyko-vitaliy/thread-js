import React, { useState } from 'react';
import { Form, Button, Icon, Image } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import { NotificationManager } from 'react-notifications';

import styles from './styles.module.scss';

const PostForm = ({ body: postBody, image: postImage, savePost, uploadImage }) => {
  const [body, setBody] = useState(postBody);
  const [image, setImage] = useState(postImage);
  const [isUploading, setIsUploading] = useState(false);

  const handleUploadFile = async ({ target }) => {
    setIsUploading(true);
    try {
      const { id, link } = await uploadImage(target.files[0]);
      setImage({ id, link });
    } catch (error) {
      NotificationManager.error('Image loading failed!');
    } finally {
      setIsUploading(false);
    }
  };

  const clearForm = () => {
    setBody('');
    setImage(null);
  };

  const handleSubmit = async () => {
    if (!body) {
      return;
    }

    try {
      await savePost({ imageId: image?.id, body }, clearForm);
      NotificationManager.info('Post was created or updated successfully!');
    } catch (error) {
      NotificationManager.error('Creating or updating post was failed!');
    }
  };

  return (
    <Form onSubmit={handleSubmit}>
      <Form.TextArea
        name="body"
        value={body}
        placeholder="What is the news?"
        onChange={ev => setBody(ev.target.value)}
      />
      {image?.link && (
        <div className={styles.imageWrapper}>
          <Image className={styles.image} src={image?.link} alt="post" />
        </div>
      )}
      <Button color="teal" icon labelPosition="left" as="label" loading={isUploading} disabled={isUploading}>
        <Icon name="image" />
        Attach image
        <input name="image" type="file" onChange={handleUploadFile} hidden />
      </Button>
      <Button floated="right" color="blue" type="submit">Post</Button>
    </Form>

  );
};

PostForm.defaultProps = {
  body: '',
  image: null
};

PostForm.propTypes = {
  uploadImage: PropTypes.func.isRequired,
  savePost: PropTypes.func.isRequired,
  body: PropTypes.string,
  image: PropTypes.shape({
    id: PropTypes.string.isRequired,
    link: PropTypes.string.isRequired
  })
};

export default PostForm;
