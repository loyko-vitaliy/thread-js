import React from 'react';
import { Popup, Image, List } from 'semantic-ui-react';
import PropTypes from 'prop-types';

import { getUserImgLink } from '../../../helpers/imageHelper';
import styles from './styles.module.scss';

const AvatarList = ({ reactions }) => (
  <List horizontal size="massive" className={styles.list}>
    {reactions.map(({ id, user }) => {
      const { username, image } = user;

      return (
        <List.Item as="a" className={styles.listItem} key={id}>
          <List.Content>
            <Popup
              trigger={<Image avatar src={getUserImgLink(image)} className={styles.avatar} />}
              position="bottom center"
              size="small"
              inverted
              style={{ padding: '3px 8px' }}
            >
              <Popup.Content>{username}</Popup.Content>
            </Popup>
          </List.Content>
        </List.Item>
      );
    })}
  </List>
);

AvatarList.propTypes = {
  reactions: PropTypes.arrayOf(PropTypes.any).isRequired
};

export default AvatarList;
