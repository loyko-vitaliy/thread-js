import React from 'react';
import { Modal } from 'semantic-ui-react';
import PropTypes from 'prop-types';

import AvatarList from '../AvatarList';

const ReactionModal = ({ reactions, showModal, toggleModal, title }) => (
  <Modal open={showModal} onClose={toggleModal}>
    <Modal.Header>{title}</Modal.Header>
    <Modal.Content>
      <AvatarList reactions={reactions} />
    </Modal.Content>
  </Modal>
);

ReactionModal.propTypes = {
  reactions: PropTypes.arrayOf(PropTypes.any).isRequired,
  title: PropTypes.string.isRequired,
  showModal: PropTypes.bool.isRequired,
  toggleModal: PropTypes.func.isRequired
};

export default ReactionModal;
