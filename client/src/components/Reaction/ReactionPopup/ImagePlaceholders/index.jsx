import React from 'react';
import { Placeholder } from 'semantic-ui-react';
import PropTypes from 'prop-types';

import styles from './styles.module.scss';

const ImagePlaceholders = ({ count }) => {
  const placeholders = new Array(count).fill(count);

  return (
    <div className={styles.placeholderContainer}>
      {placeholders.map(() => (
        <Placeholder className={styles.imagePlaceholder} key={Math.random()}>
          <Placeholder.Image />
        </Placeholder>
      ))}
    </div>
  );
};

ImagePlaceholders.propTypes = {
  count: PropTypes.number.isRequired
};

export default ImagePlaceholders;
