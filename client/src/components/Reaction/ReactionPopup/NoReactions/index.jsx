import React from 'react';
import { Popup } from 'semantic-ui-react';
import PropTypes from 'prop-types';

import PopupHeader from '../PopupHeader';
import ImagePlaceholders from '../ImagePlaceholders';

const NoReactions = ({ limit }) => (
  <>
    <PopupHeader title="No people reacted yet" />
    <Popup.Content>
      <ImagePlaceholders count={limit} />
    </Popup.Content>
  </>
);

NoReactions.propTypes = {
  limit: PropTypes.number.isRequired
};

export default NoReactions;
