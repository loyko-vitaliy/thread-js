import React from 'react';
import { Popup } from 'semantic-ui-react';
import PropTypes from 'prop-types';

import styles from './styles.module.scss';

const PopupHeader = ({ title }) => (
  <Popup.Header className={styles.header}>{title}</Popup.Header>
);

PopupHeader.propTypes = {
  title: PropTypes.string.isRequired
};

export default PopupHeader;
