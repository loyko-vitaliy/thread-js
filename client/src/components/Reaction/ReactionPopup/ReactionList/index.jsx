import React from 'react';
import { Popup, Button } from 'semantic-ui-react';
import PropTypes from 'prop-types';

import PopupHeader from '../PopupHeader';
import AvatarList from '../../AvatarList';
import ImagePlaceholders from '../ImagePlaceholders';

const ReactionList = ({ isLoading, title, limit, list, showMore, toggleModal }) => (
  <>
    <PopupHeader title={title} />
    <Popup.Content>
      {isLoading
        ? <ImagePlaceholders count={limit} />
        : (
          <>
            <AvatarList reactions={list} />
            {showMore && <Button color="teal" floated="right" size="small" onClick={toggleModal}>Show all</Button>}
          </>
        )}
    </Popup.Content>
  </>
);

ReactionList.propTypes = {
  title: PropTypes.string.isRequired,
  limit: PropTypes.number.isRequired,
  list: PropTypes.arrayOf(PropTypes.any).isRequired,
  isLoading: PropTypes.bool.isRequired,
  showMore: PropTypes.bool.isRequired,
  toggleModal: PropTypes.func.isRequired
};

export default ReactionList;
