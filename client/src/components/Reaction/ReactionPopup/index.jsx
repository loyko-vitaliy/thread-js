import React, { useState } from 'react';
import { Popup, Icon } from 'semantic-ui-react';
import PropTypes from 'prop-types';

import NoReactions from './NoReactions';
import ReactionList from './ReactionList';

const ReactionPopup = ({
  title,
  icon,
  reactions,
  reactionsCount,
  showModal,
  toggleModal,
  loadReactions,
  reactionLimit
}) => {
  const POPUP_DELAY = 800;

  const [isLoading, setIsLoading] = useState(false);

  const reactionPreview = reactions.slice(0, reactionLimit);
  const showMoreReactions = reactions.length > reactionLimit;

  const handleLoadReactions = async () => {
    setIsLoading(true);
    await loadReactions();
    setIsLoading(false);
  };

  const hasReactions = reactionsCount > 0;

  return (
    <Popup
      hoverable
      on="hover"
      disabled={showModal}
      mouseEnterDelay={POPUP_DELAY}
      mouseLeaveDelay={POPUP_DELAY}
      onOpen={handleLoadReactions}
      trigger={(
        <span>
          <Icon name={icon} />
          {reactionsCount}
        </span>
      )}
    >
      {hasReactions
        ? (
          <ReactionList
            title={title}
            limit={reactionLimit}
            list={reactionPreview}
            isLoading={isLoading}
            showMore={showMoreReactions}
            toggleModal={toggleModal}
          />
        )
        : <NoReactions limit={reactionLimit} />}
    </Popup>
  );
};

ReactionPopup.propTypes = {
  title: PropTypes.string.isRequired,
  icon: PropTypes.string.isRequired,
  reactions: PropTypes.arrayOf(PropTypes.any).isRequired,
  reactionsCount: PropTypes.number.isRequired,
  loadReactions: PropTypes.func.isRequired,
  showModal: PropTypes.bool.isRequired,
  toggleModal: PropTypes.func.isRequired,
  reactionLimit: PropTypes.number.isRequired
};

export default ReactionPopup;
