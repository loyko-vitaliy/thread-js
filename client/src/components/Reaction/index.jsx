import React, { useState } from 'react';
import PropTypes from 'prop-types';

import ReactionModal from './ReactionModal';
import ReactionPopup from './ReactionPopup';

const Reaction = ({ type, icon, reactions, reactionsCount, loadReactions }) => {
  const REACTION_LIMIT = 8;

  const [showModal, setShowModal] = useState(false);

  const title = `${type === 'like' ? 'Liked' : 'Disliked'} ${reactions.length} people`;

  const toggleModal = () => setShowModal(!showModal);

  return (
    <>
      <ReactionPopup
        title={title}
        icon={icon}
        reactions={reactions}
        reactionsCount={reactionsCount}
        showModal={showModal}
        toggleModal={toggleModal}
        loadReactions={loadReactions}
        reactionLimit={REACTION_LIMIT}
      />
      <ReactionModal title={title} reactions={reactions} showModal={showModal} toggleModal={toggleModal} />
    </>
  );
};

Reaction.defaultProps = {
  reactions: []
};

Reaction.propTypes = {
  type: PropTypes.string.isRequired,
  icon: PropTypes.string.isRequired,
  reactions: PropTypes.arrayOf(PropTypes.any),
  reactionsCount: PropTypes.number.isRequired,
  loadReactions: PropTypes.func.isRequired
};

export default Reaction;
