import React, { useState } from 'react';
import { Form, Button, Segment } from 'semantic-ui-react';
import { NotificationManager } from 'react-notifications';
import validator from 'validator';
import PropTypes from 'prop-types';

const RecoveryPasswordForm = ({ recoverPassword }) => {
  const [email, setEmail] = useState('');
  const [isEmailValid, setIsEmailValid] = useState(true);
  const [isLoading, setIsLoading] = useState(false);

  const emailChanged = data => {
    setEmail(data);
    setIsEmailValid(true);
  };

  const handleRecoverPassword = async () => {
    if (!isEmailValid || isLoading) {
      return;
    }
    setIsLoading(true);
    try {
      await recoverPassword({ email });
      NotificationManager.info('Link for reset the password was sended successfully!');
    } catch (error) {
      NotificationManager.error(error.message);
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <Form name="loginForm" size="large" onSubmit={handleRecoverPassword}>
      <Segment>
        <Form.Input
          fluid
          icon="at"
          iconPosition="left"
          placeholder="Email"
          type="email"
          error={!isEmailValid}
          onChange={ev => emailChanged(ev.target.value)}
          onBlur={() => setIsEmailValid(validator.isEmail(email))}
        />
        <Button type="submit" color="teal" fluid size="large" loading={isLoading} primary>
          Recover password
        </Button>
      </Segment>
    </Form>
  );
};

RecoveryPasswordForm.propTypes = {
  recoverPassword: PropTypes.func.isRequired
};

export default RecoveryPasswordForm;
