import React, { useState } from 'react';
import { Form, Button, Segment } from 'semantic-ui-react';
import { NotificationManager } from 'react-notifications';
import PropTypes from 'prop-types';

const ResetPasswordForm = ({ resetPassword }) => {
  const [password, setPassword] = useState('');
  const [isPasswordValid, setIsPasswordValid] = useState(true);
  const [isLoading, setIsLoading] = useState(false);

  const passwordChanged = data => {
    setPassword(data);
    setIsPasswordValid(true);
  };

  const handleUpdatePassword = async () => {
    setIsLoading(true);
    try {
      await resetPassword(password);
      NotificationManager.info('Password was updated successfully!');
    } catch (error) {
      NotificationManager.error('Updating password was failed!');
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <Form name="loginForm" size="large" onSubmit={handleUpdatePassword}>
      <Segment>
        <Form.Input
          fluid
          icon="lock"
          iconPosition="left"
          placeholder="Password"
          type="password"
          error={!isPasswordValid}
          onChange={ev => passwordChanged(ev.target.value)}
          onBlur={() => setIsPasswordValid(Boolean(password))}
        />
        <Button type="submit" color="teal" fluid size="large" loading={isLoading} primary>
          Update password
        </Button>
      </Segment>
    </Form>
  );
};

ResetPasswordForm.propTypes = {
  resetPassword: PropTypes.func.isRequired
};

export default ResetPasswordForm;
