import React, { useState } from 'react';
import { Form } from 'semantic-ui-react';
import validator from 'validator';
import { NotificationManager } from 'react-notifications';
import PropTypes from 'prop-types';

const ShareByEmail = ({ postId, sharePostByEmail, setShared }) => {
  const [email, setEmail] = useState('');
  const [isEmailValid, setIsEmailValid] = useState(true);
  const [isLoading, setIsLoading] = useState(false);

  const emailChanged = data => {
    setEmail(data);
    setIsEmailValid(true);
  };

  const handleSubmit = async () => {
    if (!isEmailValid || isLoading) {
      return;
    }

    setIsLoading(true);
    try {
      await sharePostByEmail({ email, postId });
      setShared(true);
      NotificationManager.info(`Share post to ${email} successfully!`);
    } catch (error) {
      NotificationManager.error(error.message);
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <Form onSubmit={handleSubmit}>
      <Form.Input
        fluid
        icon="at"
        iconPosition="left"
        name="email"
        type="email"
        placeholder="Email"
        value={email}
        error={!isEmailValid}
        action={{
          color: 'teal',
          labelPosition: 'right',
          icon: 'share',
          content: 'Share'
        }}
        onChange={event => emailChanged(event.target.value)}
        onBlur={() => setIsEmailValid(validator.isEmail(email))}
      />
    </Form>
  );
};

ShareByEmail.propTypes = {
  postId: PropTypes.string.isRequired,
  sharePostByEmail: PropTypes.func.isRequired,
  setShared: PropTypes.func.isRequired
};

export default ShareByEmail;
