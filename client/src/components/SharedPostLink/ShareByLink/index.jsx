import React, { useRef } from 'react';
import { Input } from 'semantic-ui-react';
import PropTypes from 'prop-types';

import styles from './styles.module.scss';

const ShareByLink = ({ postId, setCopied }) => {
  let input = useRef();

  const copyToClipboard = e => {
    input.select();
    document.execCommand('copy');
    e.target.focus();
    setCopied(true);
  };

  return (
    <Input
      className={styles.input}
      fluid
      action={{
        color: 'teal',
        labelPosition: 'right',
        icon: 'copy',
        content: 'Copy',
        onClick: copyToClipboard
      }}
      value={`${window.location.origin}/share/${postId}`}
      ref={ref => { input = ref; }}
    />
  );
};

ShareByLink.propTypes = {
  postId: PropTypes.string.isRequired,
  setCopied: PropTypes.func.isRequired
};

export default ShareByLink;
