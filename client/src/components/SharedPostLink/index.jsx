import React, { useState } from 'react';
import { Modal, Icon } from 'semantic-ui-react';
import PropTypes from 'prop-types';

import ShareByLink from './ShareByLink';
import ShareByEmail from './ShareByEmail';
import styles from './styles.module.scss';

const SharedPostLink = ({ postId, close, sharePostByEmail }) => {
  const [copied, setCopied] = useState(false);
  const [shared, setShared] = useState(false);

  return (
    <Modal open onClose={close}>
      <Modal.Header className={styles.header}>
        <span>Share Post</span>
        {copied && (
          <span>
            <Icon color="green" name="copy" />
            Copied
          </span>
        )}
        {shared && (
          <span>
            <Icon color="green" name="share" />
            Shared
          </span>
        )}
      </Modal.Header>
      <Modal.Content>
        <ShareByLink postId={postId} setCopied={setCopied} />
        <ShareByEmail postId={postId} sharePostByEmail={sharePostByEmail} setShared={setShared} />
      </Modal.Content>
    </Modal>
  );
};

SharedPostLink.propTypes = {
  postId: PropTypes.string.isRequired,
  close: PropTypes.func.isRequired,
  sharePostByEmail: PropTypes.func.isRequired
};

export default SharedPostLink;
