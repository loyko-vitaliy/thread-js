import React from 'react';
import PropTypes from 'prop-types';

import CommentForm from '../CommentForm';

const UpdateComment = ({ id, body, updateComment, closeModal }) => {
  const saveComment = async comment => {
    await updateComment(id, comment);
    closeModal();
  };

  return <CommentForm body={body} saveComment={saveComment} />;
};

UpdateComment.propTypes = {
  id: PropTypes.string.isRequired,
  body: PropTypes.string.isRequired,
  updateComment: PropTypes.func.isRequired,
  closeModal: PropTypes.func.isRequired
};

export default UpdateComment;
