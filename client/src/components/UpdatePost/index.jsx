import React from 'react';
import PropTypes from 'prop-types';

import PostForm from '../PostForm';

const UpdatePost = ({ id, body, image, updatePost, uploadImage, closeModal }) => {
  const savePost = async post => {
    await updatePost(id, post);
    closeModal();
  };

  return (
    <PostForm body={body} image={image} savePost={savePost} uploadImage={uploadImage} />
  );
};

UpdatePost.defaultProps = {
  image: null
};

UpdatePost.propTypes = {
  id: PropTypes.string.isRequired,
  body: PropTypes.string.isRequired,
  updatePost: PropTypes.func.isRequired,
  uploadImage: PropTypes.func.isRequired,
  closeModal: PropTypes.func.isRequired,
  image: PropTypes.shape({
    id: PropTypes.string.isRequired,
    link: PropTypes.string.isRequired
  })
};

export default UpdatePost;
