import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Modal, Comment as CommentUI, Header } from 'semantic-ui-react';
import moment from 'moment';
import {
  likePost,
  dislikePost,
  toggleExpandedPost,
  addComment,
  updatePost,
  deletePost,
  updateComment,
  deleteComment,
  likeComment,
  dislikeComment,
  loadPostReactions,
  loadCommentReactions
} from 'src/containers/Thread/actions';
import Post from 'src/components/Post';
import Comment from 'src/components/Comment';
import AddComment from 'src/components/AddComment';
import Spinner from 'src/components/Spinner';

const ExpandedPost = ({
  isPostBelongsToUser,
  isCommentBelongsToUser,
  post,
  sharePost,
  likePost: like,
  dislikePost: dislike,
  toggleExpandedPost: toggle,
  addComment: add,
  updatePost: editPost,
  deletePost: removePost,
  uploadImage,
  updateComment: editComment,
  deleteComment: removeComment,
  likeComment: likeCommentHandler,
  dislikeComment: dislikeCommentHandler,
  loadPostReactions: postReactions,
  loadCommentReactions: commentReactions
}) => {
  const handleDeletePost = id => {
    removePost(id);
    toggle();
  };

  return (
    <Modal dimmer="blurring" centered={false} open onClose={() => toggle()}>
      {post
        ? (
          <Modal.Content>
            <Post
              post={post}
              likePost={like}
              dislikePost={dislike}
              toggleExpandedPost={toggle}
              sharePost={sharePost}
              isPostBelongsToUser={isPostBelongsToUser}
              updatePost={editPost}
              deletePost={handleDeletePost}
              uploadImage={uploadImage}
              loadPostReactions={postReactions}
            />
            <CommentUI.Group style={{ maxWidth: '100%' }}>
              <Header as="h3" dividing>
                Comments
              </Header>
              {post.comments && post.comments
                .sort((c1, c2) => moment(c1.createdAt).diff(c2.createdAt))
                .map(comment => (
                  <Comment
                    key={comment.id}
                    comment={comment}
                    updateComment={editComment}
                    deleteComment={removeComment}
                    isCommentBelongsToUser={isCommentBelongsToUser(comment)}
                    likeComment={likeCommentHandler}
                    dislikeComment={dislikeCommentHandler}
                    loadCommentReactions={commentReactions}
                  />
                ))}
              <AddComment postId={post.id} addComment={add} />
            </CommentUI.Group>
          </Modal.Content>
        )
        : <Spinner />}
    </Modal>
  );
};

ExpandedPost.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  addComment: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  isPostBelongsToUser: PropTypes.bool.isRequired,
  isCommentBelongsToUser: PropTypes.func.isRequired,
  updatePost: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired,
  uploadImage: PropTypes.func.isRequired,
  updateComment: PropTypes.func.isRequired,
  deleteComment: PropTypes.func.isRequired,
  likeComment: PropTypes.func.isRequired,
  dislikeComment: PropTypes.func.isRequired,
  loadPostReactions: PropTypes.func.isRequired,
  loadCommentReactions: PropTypes.func.isRequired
};

const mapStateToProps = rootState => ({
  post: rootState.posts.expandedPost
});

const actions = {
  likePost,
  dislikePost,
  toggleExpandedPost,
  addComment,
  updatePost,
  deletePost,
  updateComment,
  deleteComment,
  likeComment,
  dislikeComment,
  loadPostReactions,
  loadCommentReactions
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ExpandedPost);
