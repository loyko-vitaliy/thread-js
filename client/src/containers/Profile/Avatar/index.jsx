import React, { useState, useCallback } from 'react';
import { Modal, Button, Icon, Image, Grid } from 'semantic-ui-react';
import { NotificationManager } from 'react-notifications';
import ReactCrop from 'react-image-crop';
import PropTypes from 'prop-types';
import 'react-image-crop/dist/ReactCrop.css';

import { getUserImgLink } from 'src/helpers/imageHelper';
import { getCroppedImg } from 'src/helpers/cropImageHelper';
import styles from './styles.module.scss';

const Avatar = ({ user, uploadUserAvatar, updateUserInfo }) => {
  const AVATAR_SIZE = {
    max: { width: 200, height: 200 },
    min: { width: 100, height: 100 }
  };

  const [isUploading, setIsUploading] = useState(false);
  const [loadedImage, setLoadedImage] = useState(null);
  const [imageSrc, setImageSrc] = useState(null);
  const [crop, setCrop] = useState(null);

  const [showModal, setShowModal] = useState(false);

  const toggleModal = () => setShowModal(!showModal);

  const handleOnImageLoaded = useCallback(image => {
    setLoadedImage(image);

    const { width, height } = AVATAR_SIZE.max;
    const x = (image.width - width) / 2;
    const y = (image.height - height) / 2;

    setCrop({ unit: 'px', x, y, width, height, aspect: 1 });
    return false;
  }, [AVATAR_SIZE.max]);

  const handleUploadAvatar = async ({ target: { files } }) => {
    if (files && files.length > 0) {
      setImageSrc(URL.createObjectURL(files[0]));
      toggleModal();
    }
  };

  const handleUpdateUserAvatar = async () => {
    try {
      setIsUploading(true);
      const croppedImageBlob = await getCroppedImg(loadedImage, crop);
      const croppedImageFile = new File([croppedImageBlob], 'avatar', { type: croppedImageBlob.type });
      const { id: imageId } = await uploadUserAvatar(croppedImageFile);
      await updateUserInfo(user.id, { imageId });
      toggleModal();
      NotificationManager.info('Avatar uploaded successfully!');
    } catch (error) {
      NotificationManager.info('Uploading avatar failed!');
    } finally {
      setIsUploading(false);
    }
  };

  return (
    <>
      <Grid textAlign="center">
        <Grid.Column>
          <div className={styles.userAvatar}>
            <div className={styles.avatarContainer}>
              <Image centered circular src={getUserImgLink(user.image)} alt="avatar" />
            </div>
            <Button color="teal" icon labelPosition="left" as="label">
              <Icon name="image" />
              Update avatar
              <input name="image" accept="image/*" type="file" onChange={handleUploadAvatar} hidden />
            </Button>
          </div>
        </Grid.Column>
      </Grid>
      <Modal open={showModal} size="mini" basic onClose={toggleModal}>
        <Modal.Content>
          <ReactCrop
            src={imageSrc}
            crop={crop}
            maxWidth={AVATAR_SIZE.max.width}
            maxHeight={AVATAR_SIZE.max.height}
            minWidth={AVATAR_SIZE.min.width}
            minHeight={AVATAR_SIZE.min.height}
            onChange={setCrop}
            onImageLoaded={handleOnImageLoaded}
          />
          <Button color="teal" floated="right" onClick={handleUpdateUserAvatar} loading={isUploading}>Save</Button>
        </Modal.Content>
      </Modal>
    </>
  );
};

Avatar.propTypes = {
  user: PropTypes.objectOf(PropTypes.any).isRequired,
  uploadUserAvatar: PropTypes.func.isRequired,
  updateUserInfo: PropTypes.func.isRequired
};

export default Avatar;
