import React, { useState } from 'react';
import { Grid, Input, Form, Button } from 'semantic-ui-react';
import { NotificationManager } from 'react-notifications';
import PropTypes from 'prop-types';

import styles from './styles.module.scss';

const UserInfo = ({ user, updateUserInfo }) => {
  const [username, setUsername] = useState(user.username);
  const [status, setStatus] = useState(user.status);

  const handleUpdateUser = async () => {
    try {
      await updateUserInfo(user.id, { username, status });
      NotificationManager.info('User updated successfully!');
    } catch (error) {
      NotificationManager.error(error.message);
    }
  };

  return (
    <Grid container textAlign="center">
      <Grid.Column>
        <Form onSubmit={handleUpdateUser}>
          <div className={styles.fields}>
            <div className={styles.field}>
              <Input
                fluid
                className={styles.input}
                icon="user"
                iconPosition="left"
                placeholder="username"
                type="text"
                value={username}
                onChange={event => setUsername(event.target.value)}
              />
            </div>
            <div className={styles.field}>
              <Input
                fluid
                className={styles.input}
                icon="at"
                iconPosition="left"
                placeholder="Email"
                type="email"
                value={user.email}
                disabled
              />
            </div>
            <div className={styles.field}>
              <Input
                fluid
                className={styles.input}
                icon="calendar"
                iconPosition="left"
                placeholder="status"
                type="text"
                value={status}
                onChange={event => setStatus(event.target.value)}
              />
            </div>
            <div>
              <Button floated="right" color="blue" type="submit">Save</Button>
            </div>
          </div>
        </Form>
      </Grid.Column>
    </Grid>
  );
};

UserInfo.propTypes = {
  user: PropTypes.objectOf(PropTypes.any).isRequired,
  updateUserInfo: PropTypes.func.isRequired
};

export default UserInfo;
