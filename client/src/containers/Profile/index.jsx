import React from 'react';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Grid } from 'semantic-ui-react';

import * as imageService from 'src/services/imageService';
import { updateUserInfo } from './actions';
import Avatar from './Avatar';
import UserInfo from './UserInfo';

const Profile = ({ user, updateUserInfo: editUserInfo }) => {
  const uploadUserAvatar = file => imageService.uploadImage(file);

  return (
    <Grid centered>
      <Grid.Column mobile={16} tablet={8} computer={6}>
        <Avatar user={user} uploadUserAvatar={uploadUserAvatar} updateUserInfo={editUserInfo} />
        <UserInfo user={user} updateUserInfo={editUserInfo} />
      </Grid.Column>
    </Grid>
  );
};

Profile.propTypes = {
  user: PropTypes.objectOf(PropTypes.any),
  updateUserInfo: PropTypes.func.isRequired
};

Profile.defaultProps = {
  user: {}
};

const mapStateToProps = rootState => ({
  user: rootState.profile.user
});

const actions = {
  updateUserInfo
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
