import React from 'react';
import { Grid, Header } from 'semantic-ui-react';
import { recoverPassword } from 'src/services/authService';

import Logo from 'src/components/Logo';
import RecoveryPasswordForm from 'src/components/RecoveryPasswordForm';

const RecoveryPasswordPage = () => (
  <Grid textAlign="center" verticalAlign="middle" className="fill">
    <Grid.Column style={{ maxWidth: 450 }}>
      <Logo />
      <Header as="h2" color="teal" textAlign="center">
        Recover your password
      </Header>
      <RecoveryPasswordForm recoverPassword={recoverPassword} />
    </Grid.Column>
  </Grid>
);

export default RecoveryPasswordPage;
