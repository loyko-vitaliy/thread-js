import React, { useState, useEffect } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { useLocation } from 'react-router-dom';
import { Grid, Header, Message } from 'semantic-ui-react';
import { NotificationManager } from 'react-notifications';
import PropTypes from 'prop-types';

import Spinner from 'src/components/Spinner';
import Logo from 'src/components/Logo';
import ResetPasswordForm from 'src/components/ResetPasswordForm';
import { checkResetPasswordToken } from 'src/services/authService';
import { resetPassword } from 'src/containers/Profile/actions';

const ResetPasswordPage = ({ resetPassword: updatePassword }) => {
  const { search } = useLocation();
  const query = new URLSearchParams(search);
  const token = query.get('token');

  const [isLoading, setIsLoading] = useState(false);
  const [isValidToken, setIsValidToken] = useState(false);

  useEffect(() => {
    setIsLoading(true);
    (async () => {
      try {
        await checkResetPasswordToken(token);
        setIsValidToken(true);
      } catch (error) {
        NotificationManager.error(error.message);
      } finally {
        setIsLoading(false);
      }
    })();
  }, [token]);

  const handleUpdatePassword = resetPasswordToken => password => {
    updatePassword(resetPasswordToken, { password });
  };

  return isLoading ? <Spinner /> : (
    <Grid textAlign="center" verticalAlign="middle" className="fill">
      <Grid.Column style={{ maxWidth: 450 }}>
        <Logo />
        {isValidToken
          ? (
            <>
              <Header as="h2" color="teal" textAlign="center">
                Update your password
              </Header>
              <ResetPasswordForm resetPassword={handleUpdatePassword(token)} />
            </>
          )
          : (
            <Message negative>
              <Message.Header>Wrong reset password token provided!</Message.Header>
            </Message>
          )}
      </Grid.Column>
    </Grid>
  );
};

ResetPasswordPage.propTypes = {
  resetPassword: PropTypes.func.isRequired
};

const actions = { resetPassword };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(null, mapDispatchToProps)(ResetPasswordPage);
