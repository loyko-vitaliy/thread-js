import * as postService from 'src/services/postService';
import * as commentService from 'src/services/commentService';
import { ADD_POST, UPDATE_POST, LOAD_MORE_POSTS, SET_ALL_POSTS, SET_EXPANDED_POST, DELETE_POST } from './actionTypes';

const setPostsAction = posts => ({ type: SET_ALL_POSTS, posts });
const addMorePostsAction = posts => ({ type: LOAD_MORE_POSTS, posts });
const addPostAction = post => ({ type: ADD_POST, post });
const updatePostAction = post => ({ type: UPDATE_POST, post });
const deletePostAction = post => ({ type: DELETE_POST, post });
const setExpandedPostAction = post => ({ type: SET_EXPANDED_POST, post });

export const loadPosts = filter => async dispatch => {
  const posts = await postService.getAllPosts(filter);
  dispatch(setPostsAction(posts));
};

export const loadMorePosts = filter => async (dispatch, getRootState) => {
  const {
    posts: { posts }
  } = getRootState();
  const loadedPosts = await postService.getAllPosts(filter);
  const filteredPosts = loadedPosts.filter(post => !(posts && posts.some(loadedPost => post.id === loadedPost.id)));
  dispatch(addMorePostsAction(filteredPosts));
};

export const applyPost = postId => async dispatch => {
  const post = await postService.getPost(postId);
  dispatch(addPostAction(post));
};

export const addPost = post => async dispatch => {
  const { id } = await postService.addPost(post);
  const newPost = await postService.getPost(id);
  dispatch(addPostAction(newPost));
};

export const applyUpdatedPost = postId => async (dispatch, getRootState) => {
  const post = await postService.getPost(postId);
  dispatch(updatePostAction(post));

  const { posts: { expandedPost } } = getRootState();
  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(post));
  }
};

export const updatePost = (postId, post) => async (dispatch, getRootState) => {
  const updatedPost = await postService.updatePost(postId, post);
  dispatch(updatePostAction(updatedPost));

  const { posts: { expandedPost } } = getRootState();
  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(updatedPost));
  }
};

export const applyDeletedPost = deletedPost => (dispatch, getRootState) => {
  dispatch(deletePostAction(deletedPost));

  const { posts: { expandedPost } } = getRootState();
  if (expandedPost && expandedPost.id === deletedPost.id) {
    dispatch(setExpandedPostAction(null));
  }
};

export const deletePost = postId => async dispatch => {
  const deletedPost = await postService.deletePost(postId);
  dispatch(deletePostAction(deletedPost));
};

export const toggleExpandedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setExpandedPostAction(post));
};

export const likePost = postId => async (dispatch, getRootState) => {
  const { id } = await postService.likePost(postId);
  const diff = id ? 1 : -1; // if ID exists then the post was liked, otherwise - like was removed
  const { dislikeCount, postReactions } = await postService.getPost(postId);

  const mapLikes = post => ({
    ...post,
    likeCount: Number(post.likeCount) + diff, // diff is taken from the current closure
    dislikeCount,
    postReactions
  });

  const {
    posts: { posts, expandedPost }
  } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapLikes(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapLikes(expandedPost)));
  }
};

export const dislikePost = postId => async (dispatch, getRootState) => {
  const { id } = await postService.dislikePost(postId);
  const diff = id ? 1 : -1; // if ID exists then the post was liked, otherwise - like was removed
  const { likeCount, postReactions } = await postService.getPost(postId);

  const mapDislikes = post => ({
    ...post,
    likeCount,
    postReactions,
    dislikeCount: Number(post.dislikeCount) + diff // diff is taken from the current closure
  });

  const {
    posts: { posts, expandedPost }
  } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapDislikes(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapDislikes(expandedPost)));
  }
};

export const addComment = request => async (dispatch, getRootState) => {
  const { id } = await commentService.addComment(request);
  const comment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) + 1,
    comments: [...(post.comments || []), comment] // comment is taken from the current closure
  });

  const {
    posts: { posts, expandedPost }
  } = getRootState();
  const updated = posts.map(post => (post.id !== comment.postId ? post : mapComments(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};

export const updateComment = (commentId, comment) => async (dispatch, getRootState) => {
  const { postId } = await commentService.updateComment(commentId, comment);
  const post = await postService.getPost(postId);
  dispatch(updatePostAction(post));

  const { posts: { expandedPost } } = getRootState();
  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(post));
  }
};

export const deleteComment = commentId => async (dispatch, getRootState) => {
  const { postId } = await commentService.deleteComment(commentId);
  const post = await postService.getPost(postId);

  dispatch(updatePostAction(post));

  const { posts: { expandedPost } } = getRootState();
  if (expandedPost && expandedPost.id === post.id) {
    dispatch(setExpandedPostAction(post));
  }
};

export const applyUpdatedCommentReact = commentId => async (dispatch, getRootState) => {
  const comment = await commentService.getComment(commentId);
  const post = await postService.getPost(comment.postId);
  dispatch(updatePostAction(post));

  const { posts: { expandedPost } } = getRootState();
  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPostAction(post));
  }
};

export const likeComment = commentId => async dispatch => {
  await commentService.likeComment(commentId);
  const comment = await commentService.getComment(commentId);
  const post = await postService.getPost(comment.postId);
  dispatch(setExpandedPostAction(post));
};

export const dislikeComment = commentId => async dispatch => {
  await commentService.dislikeComment(commentId);
  const comment = await commentService.getComment(commentId);
  const post = await postService.getPost(comment.postId);
  dispatch(setExpandedPostAction(post));
};

export const loadPostReactions = postId => async (dispatch, getRootState) => {
  const post = await postService.getPost(postId);
  dispatch(updatePostAction(post));

  const { posts: { expandedPost } } = getRootState();
  if (expandedPost && expandedPost.id === post.id) {
    dispatch(setExpandedPostAction(post));
  }
};

export const loadCommentReactions = commentId => async (dispatch, getRootState) => {
  const commentWithReactions = await commentService.getComment(commentId);

  const { posts: { expandedPost } } = getRootState();

  const mapComments = post => ({
    ...post,
    comments: [...post.comments && post.comments
      .map(comment => (comment.id === commentWithReactions.id ? commentWithReactions : comment))]
  });

  if (expandedPost && expandedPost.id === commentWithReactions.postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};
