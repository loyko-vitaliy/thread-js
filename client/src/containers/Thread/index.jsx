/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import * as imageService from 'src/services/imageService';
import { sharePostByEmail } from 'src/services/mailService';
import ExpandedPost from 'src/containers/ExpandedPost';
import Post from 'src/components/Post';
import AddPost from 'src/components/AddPost';
import SharedPostLink from 'src/components/SharedPostLink';
import { Checkbox, Loader } from 'semantic-ui-react';
import InfiniteScroll from 'react-infinite-scroller';
import {
  loadPosts,
  loadMorePosts,
  likePost,
  dislikePost,
  toggleExpandedPost,
  addPost,
  updatePost,
  deletePost,
  loadPostReactions
} from './actions';

import styles from './styles.module.scss';

const postsFilter = {
  userId: null,
  showOwnPosts: false,
  hideOwnPosts: false,
  showPostsLikedByMe: false,
  from: 0,
  count: 10
};

const Thread = ({
  userId,
  loadPosts: load,
  loadMorePosts: loadMore,
  posts = [],
  expandedPost,
  hasMorePosts,
  addPost: createPost,
  updatePost: editPost,
  deletePost: removePost,
  likePost: like,
  dislikePost: dislike,
  toggleExpandedPost: toggle,
  loadPostReactions: postReactions
}) => {
  const [sharedPostId, setSharedPostId] = useState(undefined);
  const [showOwnPosts, setShowOwnPosts] = useState(false);
  const [hideOwnPosts, setHideOwnPosts] = useState(false);
  const [showPostsLikedByMe, setShowPostsLikedByMe] = useState(false);

  const applyFilterToLoadPosts = () => {
    postsFilter.from = 0;
    load(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  const toggleShowOwnPosts = () => {
    if (hideOwnPosts) {
      return;
    }

    setShowOwnPosts(!showOwnPosts);

    postsFilter.showOwnPosts = !postsFilter.showOwnPosts;
    postsFilter.userId = null;
    if ((showPostsLikedByMe && showOwnPosts) || !showOwnPosts) {
      postsFilter.userId = userId;
    }

    applyFilterToLoadPosts();
  };

  const toggleHideOwnPosts = () => {
    if (showOwnPosts) {
      return;
    }

    setHideOwnPosts(!hideOwnPosts);

    postsFilter.hideOwnPosts = !postsFilter.hideOwnPosts;
    postsFilter.userId = null;
    if ((showPostsLikedByMe && hideOwnPosts) || !hideOwnPosts) {
      postsFilter.userId = userId;
    }

    applyFilterToLoadPosts();
  };

  const toggleShowPostsLikedByMe = () => {
    setShowPostsLikedByMe(!showPostsLikedByMe);

    postsFilter.showPostsLikedByMe = !postsFilter.showPostsLikedByMe;
    postsFilter.userId = null;
    if ((showPostsLikedByMe && (showOwnPosts || hideOwnPosts)) || !showPostsLikedByMe) {
      postsFilter.userId = userId;
    }

    applyFilterToLoadPosts();
  };

  const getMorePosts = () => {
    loadMore(postsFilter);
    const { from, count } = postsFilter;
    postsFilter.from = from + count;
  };

  const sharePost = id => {
    setSharedPostId(id);
  };

  const uploadImage = file => imageService.uploadImage(file);

  const checkPostBelongsToUser = (userID, post) => post.userId === userID;
  const checkCommentBelongsToUser = userID => comment => comment.userId === userID;

  return (
    <div className={styles.threadContent}>
      <div className={styles.addPostForm}>
        <AddPost addPost={createPost} uploadImage={uploadImage} />
      </div>
      <div className={styles.toolbar}>
        <Checkbox
          className={styles.checkbox}
          toggle
          label="Show only my posts"
          checked={showOwnPosts}
          onChange={toggleShowOwnPosts}
          disabled={hideOwnPosts}
        />
        <Checkbox
          className={styles.checkbox}
          toggle
          label="Hide my posts"
          checked={hideOwnPosts}
          onChange={toggleHideOwnPosts}
          disabled={showOwnPosts}
        />
        <Checkbox
          className={styles.checkbox}
          toggle
          label="Show posts liked by me"
          checked={showPostsLikedByMe}
          onChange={toggleShowPostsLikedByMe}
        />
      </div>
      <InfiniteScroll
        pageStart={0}
        loadMore={getMorePosts}
        hasMore={hasMorePosts}
        loader={<Loader active inline="centered" key={0} />}
      >
        {posts.map(post => (
          <Post
            isPostBelongsToUser={checkPostBelongsToUser(userId, post)}
            post={post}
            likePost={like}
            dislikePost={dislike}
            toggleExpandedPost={toggle}
            sharePost={sharePost}
            key={post.id}
            uploadImage={uploadImage}
            updatePost={editPost}
            deletePost={removePost}
            loadPostReactions={postReactions}
          />
        ))}
      </InfiniteScroll>
      {expandedPost
        && (
          <ExpandedPost
            isPostBelongsToUser={checkPostBelongsToUser(userId, expandedPost)}
            isCommentBelongsToUser={checkCommentBelongsToUser(userId)}
            sharePost={sharePost}
            uploadImage={uploadImage}
          />
        )}
      {sharedPostId && (
        <SharedPostLink
          postId={sharedPostId}
          close={() => setSharedPostId(undefined)}
          sharePostByEmail={sharePostByEmail}
        />
      )}
    </div>
  );
};

Thread.propTypes = {
  posts: PropTypes.arrayOf(PropTypes.object),
  hasMorePosts: PropTypes.bool,
  expandedPost: PropTypes.objectOf(PropTypes.any),
  userId: PropTypes.string,
  loadPosts: PropTypes.func.isRequired,
  loadMorePosts: PropTypes.func.isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  addPost: PropTypes.func.isRequired,
  updatePost: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired,
  loadPostReactions: PropTypes.func.isRequired
};

Thread.defaultProps = {
  posts: [],
  hasMorePosts: true,
  expandedPost: undefined,
  userId: undefined
};

const mapStateToProps = rootState => ({
  posts: rootState.posts.posts,
  hasMorePosts: rootState.posts.hasMorePosts,
  expandedPost: rootState.posts.expandedPost,
  userId: rootState.profile.user.id
});

const actions = {
  loadPosts,
  loadMorePosts,
  likePost,
  dislikePost,
  toggleExpandedPost,
  addPost,
  updatePost,
  deletePost,
  loadPostReactions
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Thread);
