import callWebApi from 'src/helpers/webApiHelper';

export const login = async request => {
  const response = await callWebApi({
    endpoint: '/api/auth/login',
    type: 'POST',
    request
  });
  return response.json();
};

export const registration = async request => {
  const response = await callWebApi({
    endpoint: '/api/auth/register',
    type: 'POST',
    request
  });
  return response.json();
};

export const getCurrentUser = async () => {
  try {
    const response = await callWebApi({
      endpoint: '/api/auth/user',
      type: 'GET'
    });
    return response.json();
  } catch (e) {
    return null;
  }
};

export const recoverPassword = async request => {
  const response = await callWebApi({
    endpoint: '/api/auth/recover',
    type: 'POST',
    request
  });

  return response.json();
};

export const checkResetPasswordToken = async resetPasswordToken => {
  const response = await callWebApi({
    endpoint: '/api/auth/reset',
    type: 'GET',
    query: { resetPasswordToken }
  });

  return response.json();
};

export const resetPassword = async (resetPasswordToken, request) => {
  const response = await callWebApi({
    endpoint: '/api/auth/reset',
    type: 'POST',
    request,
    query: { resetPasswordToken }
  });

  return response.json();
};
