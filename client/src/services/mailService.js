import callWebApi from 'src/helpers/webApiHelper';

export const sharePostByEmail = async request => {
  const response = await callWebApi({
    endpoint: '/api/mail/share_post',
    type: 'POST',
    request
  });
  return response.json();
};
