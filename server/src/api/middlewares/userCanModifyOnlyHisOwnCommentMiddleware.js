export default commentService => async ({ params: { id }, user: { id: userId } }, res, next) => {
  try {
    const comment = await commentService.getCommentById(id);

    if (comment && comment.userId !== userId) {
      next(new Error('User can modify only his own comment!'));
    }
  } catch (error) {
    next(error);
  }

  next();
};

