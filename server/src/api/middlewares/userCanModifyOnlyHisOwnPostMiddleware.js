export default postService => async ({ params: { id }, user: { id: userId } }, res, next) => {
  try {
    const post = await postService.getPostById(id);

    if (post && post.userId !== userId) {
      next(new Error('User can modify only his own post!'));
    }
  } catch (error) {
    next(error);
  }

  next();
};

