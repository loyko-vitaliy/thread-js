import { Router } from 'express';
import * as commentService from '../services/commentService';
import userCanModifyOnlyHisOwnCommentMiddleware from '../middlewares/userCanModifyOnlyHisOwnCommentMiddleware';

const router = Router();

router
  .get('/:id', (req, res, next) => commentService.getCommentById(req.params.id)
    .then(comment => res.send(comment))
    .catch(next))
  .post('/', (req, res, next) => commentService.create(req.user.id, req.body)
    .then(comment => {
      req.io.emit('new_comment', comment);
      return res.send(comment);
    })
    .catch(next))
  .put('/react', (req, res, next) => commentService.setReaction(req.user.id, req.body)
    .then(reaction => {
      if (reaction.comment && (reaction.comment.userId !== req.user.id)) {
        if (reaction && reaction.isLike) {
          req.io.to(reaction.comment.userId).emit('comment_like', 'Your comment was liked!');
        } else {
          req.io.to(reaction.comment.userId).emit('comment_dislike', 'Your comment was disliked!');
        }
      }
      req.io.emit('comment_react', { commentId: req.body.commentId, userId: req.user.id });
      return res.send(reaction);
    })
    .catch(next))
  .put(
    '/:id',
    userCanModifyOnlyHisOwnCommentMiddleware(commentService),
    (req, res, next) => commentService.updateCommentById(req.params.id, req.body)
      .then(comment => {
        req.io.emit('updated_comment', comment);
        return res.send(comment);
      })
      .catch(next)
  )
  .delete(
    '/:id',
    userCanModifyOnlyHisOwnCommentMiddleware(commentService),
    (req, res, next) => commentService.deleteCommentById(req.params.id)
      .then(comment => {
        req.io.emit('deleted_comment', comment);
        return res.send(comment);
      })
      .catch(next)
  );

export default router;
