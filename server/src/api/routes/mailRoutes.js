import { Router } from 'express';

import * as mailService from '../services/mailService';

const router = Router();

router
  .post('/share_post', (req, res, next) => mailService.sharePostByEmail(req.body)
    .then(data => res.send(data))
    .catch(next));

export default router;
