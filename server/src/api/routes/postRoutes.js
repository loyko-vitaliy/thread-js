import { Router } from 'express';
import * as postService from '../services/postService';
import * as mailService from '../services/mailService';
import userCanModifyOnlyHisOwnPostMiddleware from '../middlewares/userCanModifyOnlyHisOwnPostMiddleware';

const router = Router();

router
  .get('/', (req, res, next) => postService.getPosts(req.query)
    .then(posts => res.send(posts))
    .catch(next))
  .get('/:id', (req, res, next) => postService.getPostById(req.params.id)
    .then(post => res.send(post))
    .catch(next))
  .post('/', (req, res, next) => postService.create(req.user.id, req.body)
    .then(post => {
      req.io.emit('new_post', post); // notify all users that a new post was created
      return res.send(post);
    })
    .catch(next))
  .put('/react', (req, res, next) => postService.setReaction(req.user.id, req.body)
    .then(reaction => {
      if (reaction.post && (reaction.post.userId !== req.user.id)) {
        if (reaction.dataValues.isLike) {
          mailService.notifyWhoLikedPostByEmail({
            postLikedUser: req.user,
            post: reaction.post
          });

          req.io.to(reaction.post.userId).emit('post_like', 'Your post was liked!');
        } else {
          req.io.to(reaction.post.userId).emit('post_dislike', 'Your post was disliked!');
        }
      }
      req.io.emit('post_react', { postId: req.body.postId, userId: req.user.id });
      return res.send(reaction);
    })
    .catch(next))
  .put(
    '/:id',
    userCanModifyOnlyHisOwnPostMiddleware(postService),
    (req, res, next) => postService.updatePostById(req.params.id, req.body)
      .then(post => {
        req.io.emit('updated_post', post);
        return res.send(post);
      })
      .catch(next)
  )
  .delete(
    '/:id',
    userCanModifyOnlyHisOwnPostMiddleware(postService),
    (req, res, next) => postService.deletePostById(req.params.id)
      .then(post => {
        req.io.emit('deleted_post', post);
        return res.send(post);
      })
      .catch(next)
  );

export default router;
