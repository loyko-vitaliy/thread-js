import { createToken } from '../../helpers/tokenHelper';
import { encrypt } from '../../helpers/cryptoHelper';
import userRepository from '../../data/repositories/userRepository';
import * as mailService from './mailService';

export const login = async ({ id }) => ({
  token: createToken({ id }),
  user: await userRepository.getUserById(id)
});

export const register = async ({ password, ...userData }) => {
  const newUser = await userRepository.addUser({
    ...userData,
    password: await encrypt(password)
  });
  return login(newUser);
};

export const recoverPassword = async ({ email }) => {
  if (!email) {
    throw new Error('Email is required!');
  }

  const user = await userRepository.getByEmail(email);
  if (!user) {
    throw new Error(`User with email ${email} was not found!`);
  }

  return mailService.sendResetPasswordLink({
    email,
    token: await userRepository.updateResetPasswordToken(user.id)
  });
};

export const checkResetPasswordToken = async resetPasswordToken => {
  if (!resetPasswordToken) {
    throw new Error('ResetPasswordToken is required!');
  }

  const user = await userRepository.getUserByResetPasswordToken(resetPasswordToken);
  if (!user || !user.resetPasswordExpires > Date.now()) {
    throw new Error('Provided token is not valid');
  }

  return { success: true };
};

export const resetPassword = async (token, password) => {
  const user = await userRepository.getUserByResetPasswordToken(token);

  const updatedUser = await userRepository.updateUserById(user.id, {
    password: await encrypt(password),
    resetPasswordToken: null,
    resetPasswordExpires: null
  });

  return login(updatedUser);
};
