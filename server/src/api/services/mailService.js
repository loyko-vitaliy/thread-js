import path from 'path';
import nunjucks from 'nunjucks';
import sgMail from '@sendgrid/mail';

import userRepository from '../../data/repositories/userRepository';
import env from '../../env';

const TEMPLATE_PATH = path.resolve(__dirname, '../../views/mail');

nunjucks.configure({ autoescape: true });
sgMail.setApiKey(env.mail.sendgridApiKey);

export const sendMail = async (template, data, { to, subject = 'This is an information message' } = {}) => {
  try {
    await sgMail.send({
      to,
      from: env.mail.mailFrom,
      subject,
      html: nunjucks.render(`${TEMPLATE_PATH}/${template}.html`, data)
    });

    return { success: true };
  } catch (error) {
    throw new Error('Unable to send email');
  }
};

export const sendResetPasswordLink = async ({ email, token }) => (
  sendMail('repair-password', {
    link: `http://${env.app.host}:${env.app.port}/reset?token=${token}`
  }, {
    to: email,
    subject: 'Password recovery mail'
  })
);

export const notifyWhoLikedPostByEmail = async ({ postLikedUser, post }) => {
  const { id: postId, user: postAuthor } = post;

  return sendMail('like-post', {
    postLikedUser,
    postAuthor,
    postLink: `http://${env.app.host}:${env.app.port}/share/${postId}`
  }, {
    to: postAuthor.email,
    subject: 'Someone liked your post!'
  });
};

export const sharePostByEmail = async ({ email, postId }) => {
  const user = await userRepository.getByEmail(email);
  if (!user) {
    throw new Error(`User with email ${email} is not exists!`);
  }

  return sendMail('share-post', {
    user,
    postLink: `http://${env.app.host}:${env.app.port}/share/${postId}`
  }, {
    to: email,
    subject: 'Shared post'
  });
};
