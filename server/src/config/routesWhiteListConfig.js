export default [
  '/auth/login',
  '/auth/register',
  '/auth/recover',
  '/auth/reset'
];
