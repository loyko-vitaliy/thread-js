import sequelize from '../db/connection';
import { CommentModel, UserModel, ImageModel, CommentReactionModel } from '../models/index';
import BaseRepository from './baseRepository';

const likeCase = bool => `(
  SELECT COALESCE(SUM(CASE WHEN "commentReaction"."isLike" = ${bool} THEN 1 ELSE 0 END), 0)
  FROM "commentReactions" AS "commentReaction"
  WHERE "comment".id = "commentReaction"."commentId"
)`;
class CommentRepository extends BaseRepository {
  getCommentById(id) {
    return this.model.findOne({
      group: [
        'comment.id',
        'user.id',
        'user->image.id',
        'commentReactions.id',
        'commentReactions->user.id',
        'commentReactions->user->image.id'
      ],
      where: { id },
      attributes: {
        include: [
          [sequelize.literal(likeCase(true)), 'likeCount'],
          [sequelize.literal(likeCase(false)), 'dislikeCount']
        ]
      },
      include: [{
        model: UserModel,
        attributes: ['id', 'username', 'status'],
        include: {
          model: ImageModel,
          attributes: ['id', 'link']
        }
      }, {
        model: CommentReactionModel,
        attributes: ['id', 'isLike'],
        include: {
          model: UserModel,
          attributes: ['id', 'username'],
          include: {
            model: ImageModel,
            attributes: ['id', 'link']
          }
        }
      }]
    });
  }

  async updateCommentById(id, comment) {
    await this.updateById(id, comment);
    return this.getCommentById(id);
  }
}

export default new CommentRepository(CommentModel);
