import { Op } from 'sequelize';
import sequelize from '../db/connection';
import {
  PostModel,
  CommentModel,
  UserModel,
  ImageModel,
  PostReactionModel,
  CommentReactionModel
} from '../models/index';
import BaseRepository from './baseRepository';

const postLikeCase = bool => `(
  SELECT COALESCE(SUM(CASE WHEN "postReaction"."isLike" = ${bool} THEN 1 ELSE 0 END), 0)
  FROM "postReactions" AS "postReaction"
  WHERE "post".id = "postReaction"."postId"
)`;

// const commentLikeCase = bool => `
//   SUM(CASE WHEN "comments->commentReactions"."isLike" = ${bool} THEN 1 ELSE 0 END)
//   OVER(PARTITION BY "comments"."id")
// `;

const commentLikeCase = bool => `(
  SELECT COALESCE(SUM(CASE WHEN "commentReaction"."isLike" = ${bool} THEN 1 ELSE 0 END), 0)
  FROM "commentReactions" AS "commentReaction"
  WHERE "comments"."id" = "commentReaction"."commentId" 
)`;

class PostRepository extends BaseRepository {
  async getPosts(filter) {
    const {
      from: offset,
      count: limit,
      userId,
      showOwnPosts = false,
      hideOwnPosts = false,
      showPostsLikedByMe = false
    } = filter;

    let useInnerJoinForPostReactions = false;

    const where = {};
    if (userId) {
      if (showOwnPosts === 'true' || (showPostsLikedByMe === 'true' && showOwnPosts === 'true')) {
        Object.assign(where, { userId });
      } else if (hideOwnPosts === 'true' || (showPostsLikedByMe === 'true' && hideOwnPosts === 'true')) {
        Object.assign(where, { userId: { [Op.ne]: userId } });
      }
    }

    const likedWhere = {};
    if (userId) {
      if (showPostsLikedByMe === 'true'
        || (showPostsLikedByMe === true && (showOwnPosts === 'true' || hideOwnPosts === 'true'))) {
        Object.assign(likedWhere, { isLike: true, userId });
        useInnerJoinForPostReactions = true;
      }
    }

    return this.model.findAll({
      where,
      attributes: {
        include: [
          [sequelize.literal(`
                          (SELECT COUNT(*)
                          FROM "comments" as "comment"
                          WHERE "post"."id" = "comment"."postId" AND "comment"."deletedAt" IS NULL)`), 'commentCount'],
          [sequelize.literal(postLikeCase(true)), 'likeCount'],
          [sequelize.literal(postLikeCase(false)), 'dislikeCount']
        ]
      },
      include: [{
        model: ImageModel,
        attributes: ['id', 'link']
      }, {
        model: UserModel,
        attributes: ['id', 'username', 'status'],
        include: {
          model: ImageModel,
          attributes: ['id', 'link']
        }
      }, {
        model: PostReactionModel,
        attributes: [],
        duplicating: false,
        where: likedWhere,
        required: useInnerJoinForPostReactions
      }],
      group: [
        'post.id',
        'image.id',
        'user.id',
        'user->image.id'
      ],
      order: [['createdAt', 'DESC']],
      offset,
      limit
    });
  }

  getPostById(id) {
    return this.model.findOne({
      group: [
        'post.id',
        'comments.id',
        'comments->user.id',
        'comments->user->image.id',
        'user.id',
        'user->image.id',
        'image.id',
        'comments->commentReactions.id',
        'postReactions.id',
        'postReactions.isLike',
        'postReactions->user.id',
        'postReactions->user->image.id'
      ],
      where: { id },
      attributes: {
        include: [
          [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "post"."id" = "comment"."postId" AND "comment"."deletedAt" IS NULL)`), 'commentCount'],
          [sequelize.literal(postLikeCase(true)), 'likeCount'],
          [sequelize.literal(postLikeCase(false)), 'dislikeCount']
        ]
      },
      include: [
        {
          model: CommentModel,
          attributes: {
            include: [
              [sequelize.literal(commentLikeCase(true)), 'likeCount'],
              [sequelize.literal(commentLikeCase(false)), 'dislikeCount']
            ]
          },
          include: [
            {
              model: UserModel,
              attributes: ['id', 'username', 'status'],
              include: {
                model: ImageModel,
                attributes: ['id', 'link']
              }
            },
            {
              model: CommentReactionModel,
              attributes: []
            }
          ]
        },
        {
          model: UserModel,
          attributes: ['id', 'username', 'status'],
          include: {
            model: ImageModel,
            attributes: ['id', 'link']
          }
        },
        {
          model: ImageModel,
          attributes: ['id', 'link']
        },
        {
          model: PostReactionModel,
          attributes: ['id', 'isLike'],
          include: {
            model: UserModel,
            attributes: ['id', 'username'],
            include: {
              model: ImageModel,
              attributes: ['id', 'link']
            }
          }
        }
      ]
    });
  }

  async updatePostById(id, post) {
    await this.updateById(id, post);
    return this.getPostById(id);
  }
}

export default new PostRepository(PostModel);
