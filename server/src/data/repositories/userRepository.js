import { v4 as uuid } from 'uuid';

import { UserModel, ImageModel } from '../models/index';
import BaseRepository from './baseRepository';

class UserRepository extends BaseRepository {
  addUser(user) {
    return this.create(user);
  }

  getByEmail(email) {
    return this.model.findOne({ where: { email } });
  }

  getByUsername(username) {
    return this.model.findOne({ where: { username } });
  }

  getUserById(id) {
    return this.model.findOne({
      group: [
        'user.id',
        'image.id'
      ],
      where: { id },
      include: {
        model: ImageModel,
        attributes: ['id', 'link']
      }
    });
  }

  getUserByResetPasswordToken(resetPasswordToken) {
    return this.model.findOne({ where: { resetPasswordToken } });
  }

  async updateUserById(userId, userData) {
    if (userData.username) {
      const userExists = await this.getByUsername(userData.username);

      if (userExists && userExists.id !== userId) {
        throw new Error('Username already exists!');
      }
    }

    await this.updateById(userId, userData);

    return this.getUserById(userId);
  }

  async updateResetPasswordToken(userId) {
    const tokenExpires = 1 * 60 * 60 * 1000;
    const resetPasswordToken = uuid();
    const resetPasswordExpires = new Date(Date.now() + tokenExpires);
    await this.updateById(userId, { resetPasswordToken, resetPasswordExpires });

    return resetPasswordToken;
  }
}

export default new UserRepository(UserModel);
